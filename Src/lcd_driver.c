
#include "stm32l1xx.h"

enum COM {
  COM0 = 0,
  COM1 = 2,
};

enum SEG {
  SEG0 = 0x1U << 0,
  SEG1 = 0x1U << 1,
  SEG2 = 0x1U << 2,
  SEG3 = 0x1U << 3,
  SEG4 = 0x1U << 4,
  SEG5 = 0x1U << 5,
  SEG6 = 0x1U << 6,
  SEG7 = 0x1U << 7,
  SEG8 = 0x1U << 8,
  SEG9 = 0x1U << 9,
  SEG10 = 0x1U << 10,
  SEG11 = 0x1U << 11,
  SEG12 = 0x1U << 12,
  SEG13 = 0x1U << 13,
  SEG14 = 0x1U << 14,
  SEG_ALL = 0x3fff,
};


#define WAIT_UPDATE() while (LCD->SR & LCD_SR_UDR){}
#define UPDATE() LCD->SR |= LCD_SR_UDR
#define SEG_ON(com,seg)  LCD->RAM[com] |= (seg)
#define SEG_OFF(com,seg)  LCD->RAM[com] &= ~(seg)

#define S_ON(segCom0,segCom1)  SEG_ON(COM0,segCom0), SEG_ON(COM1,segCom1)

#define SEG_CLEAN()   LCD->RAM[COM0] = 0x0, LCD->RAM[COM1] = 0x0

static void InitRCCPWR() {
  // Разрешаем тактирования PWR и LCD в регистре RCC_APB1ENR
  RCC->APB1ENR |= RCC_APB1ENR_PWREN | RCC_APB1ENR_LCDEN;
  //В регистре PWR_CR  снимаем защиту от записи в регистр RCC_CSR
  PWR->CR |= PWR_CR_DBP;
  //Cброс источника тактирования установкой бита RTCRST
  RCC->CSR |= RCC_CSR_RTCRST;
  //Для выбора нового источника тактирования необходимо убрать бит RTCRST
  RCC->CSR &= ~RCC_CSR_RTCRST;
  //Выбираем внутренний источник тактирования LSI
  RCC->CSR |= RCC_CSR_LSION;
  //Ждём готовности RCC LSI
  while (!(RCC->CSR & RCC_CSR_LSIRDY));

  //Выбираем LSI в качестве источника тактирования для RTC/LCD
  RCC->CSR |= RCC_CSR_RTCSEL_LSI;
}

static inline void InitPIN(GPIO_TypeDef *port, uint8_t pin) {
  //MODERx 10: Alternate function mode
  port->MODER |= (0x2U << (pin * 2));
  //OTYPER 0: Output push-pull
  port->OTYPER &= ~(0x1U << pin);
  //OSPEEDR 00: Low speed
  port->OSPEEDR &= ~(0x3U << (pin * 2));
  //PUPDR 00: No pull-up, pull-down
  port->PUPDR &= ~(0x3U << (pin * 2));
  // AFIO11 - LCD  AF11:1011(0xB)
  uint8_t i = (pin < 8 ? 0 : 1);
  uint8_t pPos = ((pin - i * 8)*4);
  //PIN (i==0) pin 0-7 (i==1) pin 8-15
  port->AFR[i] &= ~(0xFU << pPos);
  port->AFR[i] |= (0xB << pPos);

}

static void InitGPIO() {
  /**LCD GPIO Configuration
PA1     ------> LCD_SEG0
PA2     ------> LCD_SEG1
PA3     ------> LCD_SEG2
PA6     ------> LCD_SEG3
PA7     ------> LCD_SEG4
PA8     ------> LCD_COM0
PA9     ------> LCD_COM1

PB0     ------> LCD_SEG5
PB1     ------> LCD_SEG6
PB3     ------> LCD_SEG7
PB4     ------> LCD_SEG8
PB5     ------> LCD_SEG9
PB10    ------> LCD_SEG10
PB11    ------> LCD_SEG11
PB12    ------> LCD_SEG12
PB13    ------> LCD_SEG13
PB14    ------> LCD_SEG14
   */

  //Тактирование портов A и B
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;

  InitPIN(GPIOA, 1); // PA1     ------> LCD_SEG0
  InitPIN(GPIOA, 2); // PA2     ------> LCD_SEG1
  InitPIN(GPIOA, 3); // PA3     ------> LCD_SEG2
  InitPIN(GPIOA, 6); // PA6     ------> LCD_SEG3
  InitPIN(GPIOA, 7); // PA7     ------> LCD_SEG4
  InitPIN(GPIOA, 8); // PA8     ------> LCD_COM0
  InitPIN(GPIOA, 9); // PA9     ------> LCD_COM1

  InitPIN(GPIOB, 0); // PB0     ------> LCD_SEG5
  InitPIN(GPIOB, 1); // PB1     ------> LCD_SEG6
  InitPIN(GPIOB, 3); // PB3     ------> LCD_SEG7
  InitPIN(GPIOB, 4); // PB4     ------> LCD_SEG8
  InitPIN(GPIOB, 5); // PB5     ------> LCD_SEG9
  InitPIN(GPIOB, 10); //PB10    ------> LCD_SEG10
  InitPIN(GPIOB, 11); //PB11    ------> LCD_SEG11
  InitPIN(GPIOB, 12); //PB12    ------> LCD_SEG12
  InitPIN(GPIOB, 13); //PB13    ------> LCD_SEG13
  InitPIN(GPIOB, 14); //PB14    ------> LCD_SEG14

}

static void InitLCD() {
  //00: Bias 1/4
  //01: Bias 1/2
  //10: Bias 1/3
  LCD->CR &= ~LCD_CR_BIAS;
  //  LCD->CR |= LCD_CR_BIAS_1;
  //001: 1/2 duty
  //010: 1/3 duty
  //011: 1/4 duty
  //100: 1/8 duty
  LCD->CR &= ~LCD_CR_DUTY;
  LCD->CR |= LCD_CR_DUTY_0;
  //0: SEG pin multiplexing disabled
  LCD->CR &= ~LCD_CR_MUX_SEG;
  //0000: ck_ps = LCDCLK
  LCD->FCR &= ~LCD_FCR_PS;
  //0000: ck_div = ck_ps/16
  LCD->FCR &= ~LCD_FCR_DIV;
  LCD->FCR |= 0x1040000; // 0x1040000 = 1 0000 0100 0000 0000 0000 0000  ck_ps = LCDCLK/16, ck_div = ck_ps/17.
  //  000: VLCD0  100: VLCD4
  //  001: VLCD1  101: VLCD5
  //  010: VLCD2  110: VLCD6
  //  011: VLCD3  111: VLCD7
  LCD->FCR &= ~LCD_FCR_CC;
  LCD->FCR |= (LCD_FCR_CC_2); //LCD_FCR_CC_2 | LCD_FCR_CC_1 | LCD_FCR_CC_0
  while (!(LCD->SR & LCD_SR_FCRSR));

  //0: Internal source (voltage step-up converter)
  LCD->CR &= ~LCD_CR_VSEL;
  //1: LCD Controller enabled
  LCD->CR |= LCD_CR_LCDEN;
  while (!(LCD->SR & LCD_SR_RDY));
  while (!(LCD->SR & LCD_SR_ENS));

}

void LCD_Clean() {
  WAIT_UPDATE();
  SEG_CLEAN();
  UPDATE();
}

void LCD_Init() {
  InitRCCPWR();
  InitGPIO();
  InitLCD();
  LCD_Clean();
}

/**
 * Генератор цифр
 * @param sVL левая вертикальная пара сегментов
 * @param sVR правая вертикальная пара сегментов
 * @param sHT верхняя и средняя горизонтальная сегментов
 * @param sHB нижний горизонтальный сегмент
 * @param d цифра
 */
static void SetDigitR(enum SEG sVL, enum SEG sVR, enum SEG sHT, enum SEG sHB, uint8_t d) {
  switch (d) {
    case 0:
      S_ON(sVL | sVR | sHT, sVL | sVR | sHB);
      break;
    case 1:
      S_ON(sVR, sVR);
      break;
    case 2:
      S_ON(sHT | sVR, sHT | sVL | sHB);
      break;
    case 3:
      S_ON(sVR | sHT, sHT | sVR | sHB);
      break;
    case 4:
      S_ON(sVL | sVR, sVR | sHT);
      break;
    case 5:
      S_ON(sVL | sHT, sVR | sHB | sHT);
      break;
    case 6:
      S_ON(sHT | sVL, sVL | sHB | sHT | sVR);
      break;
    case 7:
      S_ON(sVR | sHT, sVR);
      break;
    case 8:
      S_ON(sVL | sVR | sHT, sVL | sVR | sHB | sHT);
      break;
    case 9:
      S_ON(sVL | sVR | sHT, sVR | sHB | sHT);
      break;
    default:
      break;
  }
}

static void SetDigitR0(uint8_t d) {
  SetDigitR(SEG10, SEG12, SEG11, SEG5, d);
}

static void SetDigitR1(uint8_t d) {
  SetDigitR(SEG6, SEG8, SEG7, SEG9, d);
}

static void SetDigitR2(uint8_t d) {
  SetDigitR(SEG2, SEG4, SEG3, SEG1, d);
}

static void SetDigitR3(uint8_t d) {
  if (d == 1) {
    SEG_ON(COM0, SEG1);
  }
}

void LCD_SetVal(uint16_t val) {
  WAIT_UPDATE();
  SEG_CLEAN();
  if (val <= 1999) {
    uint16_t v = val;
    SetDigitR0(v % 10);

    v /= 10;
    if (v > 0) {
      SetDigitR1(v % 10);

      v /= 10;
      if (v > 0) {
        SetDigitR2(v % 10);

        v /= 10;
        if (v > 0) {
          SetDigitR3(v % 10);
        }
      }
    }
  }
  UPDATE();
}

#define PAUSE(p)   for (volatile int i = 0; i < (p*1000000); i++);

void LCD_Test() {

  while (1) {

    WAIT_UPDATE();
    LCD_SetVal(0);
    UPDATE();
    PAUSE(2);

    for (uint16_t i = 1; i <= 1000; i *= 10) {
      WAIT_UPDATE();
      LCD_SetVal(i);
      UPDATE();
      PAUSE(1);

    }

    for (uint16_t i = 1000; i <= 1999; i += 111) {
      WAIT_UPDATE();
      LCD_SetVal(i);
      UPDATE();
      PAUSE(1);
    }

    PAUSE(1);
    WAIT_UPDATE();
        
    SEG_ON(COM0, SEG0); //IN
    SEG_ON(COM1, SEG0); //OUT

    SEG_ON(COM0, SEG9); //point
    SEG_ON(COM0, SEG5); //dpoint

    SEG_ON(COM0, SEG13); //F C E
    SEG_ON(COM1, SEG13); //F C E
    SEG_ON(COM0, SEG14); //F C E
    SEG_ON(COM1, SEG14); //F C E

    //SEG_ON(COM0, SEG_ALL);
    //SEG_ON(COM1, SEG_ALL);

    UPDATE();
    PAUSE(2);



  }

}

